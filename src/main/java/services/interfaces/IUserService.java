package services.interfaces;

import domain.User;

public interface IUserService {
    public User getUserById(long id);
}
