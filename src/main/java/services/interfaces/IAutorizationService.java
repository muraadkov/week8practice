package services.interfaces;

import domain.AccessToken;
import domain.LoginData;

public interface IAutorizationService {
    public AccessToken authenticate(LoginData data) throws Exception;
}
