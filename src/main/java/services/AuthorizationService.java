package services;

import domain.AccessToken;
import domain.LoginData;
import domain.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import repositories.UserRepository;
import repositories.interfaces.IUserRepository;
import services.interfaces.IAutorizationService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Random;

public class AuthorizationService implements IAutorizationService {
    private final IUserRepository userRepo = new UserRepository();

    @Override
    public AccessToken authenticate(LoginData data) throws Exception {
        User user = signIn(data);
        AccessToken token = new AccessToken(getToken(user));
        return token;
    }

    private User signIn(LoginData data) throws Exception {
        User user = userRepo.getUserByLogin(data);
        if(user == null){
            throw new Exception("User does not exist");
        }
        return user;
    }

    private String getToken(User user){
        Instant now = Instant.now();
        String secretWord = "TheStrongestSecretKeyICanThinkOf";
        return Jwts.builder()
                .setIssuer(user.getUsername())
                .setIssuedAt(Date.from(now))
                .claim("1d20", new Random().nextInt(20) + 1)
                .setExpiration(Date.from(now.plus(10, ChronoUnit.MINUTES)))
                .signWith(Keys.hmacShaKeyFor(secretWord.getBytes()))
                .compact();

    }

}
