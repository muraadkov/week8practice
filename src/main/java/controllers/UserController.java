package controllers;

import javax.ws.rs.*;

import domain.User;
import services.UserService;
import services.interfaces.IUserService;

import javax.ws.rs.core.Response;

@Path("users")
public class UserController {
    private final IUserService userService = new UserService();
    @GET
    public String index(){
        return "Hello from UserController";
    }
    
    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") long id) {
        User user;
        try {
            user = userService.getUserById(id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be created").build();
        }


        if (user == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(user)
                    .build();
        }
    }

}
