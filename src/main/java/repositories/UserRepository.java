package repositories;

import domain.LoginData;
import domain.User;
import repositories.interfaces.IDBRepository;
import repositories.interfaces.IUserRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class UserRepository implements IUserRepository {
    private final IDBRepository dbRepo = new PostgresRepository();
    @Override
    public void add(User entity) {

    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void remove(User entity) {

    }

    @Override
    public List<User> query(String sql) {
        return null;
    }

    @Override
    public User queryOne(String sql) {
        try {
            Statement stmt = dbRepo.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if(rs.next()){
                User user = new User(
                        rs.getLong("id"),
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getString("username"),
                        rs.getDate("birthdayDate")
                );
                return user;
            }
        } catch (SQLException e) {
            throw new BadRequestException();
        }
        return null;
    }

    @Override
    public User getUserById(long id){
        String sql = "select * from users where id = " + id + " limit 1";
        return queryOne(sql);
     }

    @Override
    public User getUserByLogin(LoginData data) {
        String sql = "select * from users where username = ? and password = ? limit 1";
        try {
            PreparedStatement stmt = dbRepo.getConnection().prepareStatement(sql);
            stmt.setString(1, data.getUsername());
            stmt.setString(2, data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
               User user = new User(
                       rs.getLong("id"),
                       rs.getString("fname"),
                       rs.getString("lname"),
                       rs.getString("username"),
                       rs.getString("password"),
                       rs.getDate("birthdayDate")
               );
               return user;
            }
        } catch (SQLException e) {
            throw new BadRequestException();
        }
        return null;
    }
}
