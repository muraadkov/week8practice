package repositories.interfaces;

import domain.LoginData;
import domain.User;

public interface IUserRepository extends IEntityRepository<User> {
    User getUserById(long id);

    User getUserByLogin(LoginData data);
}
