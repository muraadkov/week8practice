package domain;

import java.util.Date;

public class User {
    private long id;
    private String fname;
    private String lname;
    private String username;
    private String password;
    private Date birthdayDate;

    public User(long id, String fname, String lname, String username, String password, Date birthdayDate){
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.password = password;
        this.birthdayDate = birthdayDate;
    }

    public User(long id, String fname, String lname, String username, Date birthdayDate){
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.birthdayDate = birthdayDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }




}
